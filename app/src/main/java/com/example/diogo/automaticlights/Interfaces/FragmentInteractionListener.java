package com.example.diogo.automaticlights.Interfaces;

import com.example.diogo.automaticlights.Enumerations.Task;

/**
 * Created by diogo on 18-03-2018.
 */

public interface FragmentInteractionListener {

    void doTask(Task t);

}
