package com.example.diogo.automaticlights;

import com.example.diogo.automaticlights.Enumerations.LightState;

public class AppCommons {

    /**
     * WHATs used in android.os.Messages
     */
    public static final int toMain_WHAT = 0;
    public static final int updateLightState_WHAT = 1;
    public static final int enableButton_WHAT = 2;
    public static final int textUpdate_WHAT = 3;
    public static final int disable_button_WHAT = 4;
    public static final int toLogin_WHAT = 5;

    /**
     * Variables to keep track of device's current user and light state
     */
    private static String currentUserName;
    private static LightState currentLightState;


    public static String getCurrentUserName() {
        return currentUserName;
    }

    public static void setCurrentUserName(String currentUserName) {
        AppCommons.currentUserName = currentUserName;
    }

    public static LightState getCurrentLightState() {
        return currentLightState;
    }

    public static void setCurrentLightState(LightState currentLightState) {
        AppCommons.currentLightState = currentLightState;
    }

    public static void init(){
        currentUserName = "";
    }
}
