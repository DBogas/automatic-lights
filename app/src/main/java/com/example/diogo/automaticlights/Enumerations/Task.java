package com.example.diogo.automaticlights.Enumerations;

/**
 * Created by diogo on 22-03-2018.
 */

public enum Task {
    LOGIN,
    HIT_LIGHTS,
    LOGOUT
}
