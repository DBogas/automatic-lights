package com.example.diogo.automaticlights.Enumerations;

/**
 * Created by diogo on 18-03-2018.
 */

public enum FragmentState {
    LOGIN,
    MAIN,
    DEBUG
}
