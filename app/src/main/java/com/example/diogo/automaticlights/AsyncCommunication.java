package com.example.diogo.automaticlights;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.example.diogo.automaticlights.Enumerations.LightState;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Communication with RaspberryPi
 */
public class AsyncCommunication extends AsyncTask<String, String, Void> {

    private final static String TAG = "AsyncCommunication";

    private String destinationAddress;
    private int destinationPort;

    private Socket socket;
    private PrintWriter out;

    private Context parentContext;

    private Handler msgHandler;

    private boolean isExecutting;
    private boolean isConnected;


    public AsyncCommunication(String destinationAddress, int destinationPort, Context parentContext, Handler msgHandler) {
        this.destinationAddress = destinationAddress;
        this.destinationPort = destinationPort;
        this.parentContext = parentContext;
        this.msgHandler = msgHandler;
        this.isExecutting = false;
        this.isConnected = false;
    }


    public void closeSocket() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isExecutting() {
        return isExecutting;
    }

    public void setExecutting(boolean executting) {
        isExecutting = executting;
    }

    public boolean isConnected() {
        return isConnected;
    }

    @Override
    protected void onPreExecute() {
        /**
         * Disable login button until connection is established!
         */
       /* Bundle data = new Bundle();
        data.putString("button", "login");
        sendMessage(AppCommons.disable_button_WHAT, data);*/
    }

    /**
     * Method to send messages to the server.
     * This creates/connects the socket if needed.
     *
     * @param message
     */
    public void sendMessage(final String message) {
        Log.i(TAG, "Sending message on new thread: " + message);
        Log.i(TAG, "-----------------------------------------");
        Thread messageThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (socket.isConnected()) {
                    try {
                        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                        out.print(message);
                        out.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                //Toast.makeText(parentContext, "Connection is not established yet", Toast.LENGTH_SHORT).show();
            }

        });
        messageThread.start();
    }


    /**
     * Listens to messages from the server.
     * Doesn't close the socket or the inputStream as long as it is listening.
     * Also verifies if the socket is null or not connected, but this is done, also,  on login.
     *
     * @param arg0
     * @return
     */
    @Override
    protected Void doInBackground(String... arg0) {
        try {
            Log.i(TAG, "Creating socket!");

            socket = new Socket();
            while (!socket.isConnected()) {
                socket.connect(new InetSocketAddress(destinationAddress, destinationPort));
                Log.i(TAG, "Connected: " + socket.isConnected());
            }
            isConnected = true;
            Log.i(TAG, "Socket should be connected, enabling login!");
            Bundle data = new Bundle();
            data.putString("button", "login");
            sendMessage(AppCommons.enableButton_WHAT, data);


            InputStream inpStr = socket.getInputStream();
            ByteArrayOutputStream byteArr = new ByteArrayOutputStream(1024);
            byte[] buffer = new byte[1024];

            int bytesRead;
            while ((bytesRead = inpStr.read(buffer)) != -1) {
                byteArr.write(buffer, 0, bytesRead);
                if (byteArr.toString() != null || byteArr.toString() != "") {
                    String response = byteArr.toString();
                    byteArr.reset(); // needed so messages dont accumulate
                    //Toast.makeText(parentContext, "message: "+response, Toast.LENGTH_SHORT).show();
                    publishProgress(response);
                }

            }
            Log.d(TAG, "Closing socket!");
            socket.close();
            sendMessage(AppCommons.toLogin_WHAT,null);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Method that is called when we receive something from the server.
     * Updates the system interactions in the main screen.
     * <p>
     * <p>
     *
     * @param serverAnswer
     */
    @Override
    protected void onProgressUpdate(String... serverAnswer) {
        Log.i(TAG, "Updating progress...");
        String response = serverAnswer[0];
        String all = "";
        for(String s : serverAnswer)
            all += s;
        Log.i(TAG, "Received mesage: " + all);

        Bundle data = new Bundle(); // updates textuais
        Bundle lightStateBundle; // updates ao estado da luz
        Bundle buttonStateBundle;


        switch (response) {

            /**
             * Case where the client's login is accepted
             */
            case "loginAccepted":
                sendMessage(AppCommons.toMain_WHAT, null);
                break;

            /**
             * Case where the client's login is refused
             */
            case "loginRefused":
                Toast.makeText(parentContext, "Your login was refused!", Toast.LENGTH_SHORT);
                Log.i(TAG, "Login was refused.");
                break;

            /**
             * Case when the client asks to switch on the lights and they are already on.
             * Mainly for synchronization purposes.
             * Gives a textual update.
             * Updates the light state variables.
             * Enables the button
             */
            case "lightsAlreadyOn":

                data.putString("msg", "Lights were already on!");
                sendMessage(AppCommons.textUpdate_WHAT, data);

                lightStateBundle = new Bundle();
                lightStateBundle.putString("msg", "ON");
                sendMessage(AppCommons.updateLightState_WHAT, lightStateBundle);

                buttonStateBundle = new Bundle();
                buttonStateBundle.putString("button", "hit");
                sendMessage(AppCommons.enableButton_WHAT, buttonStateBundle);
                break;

            /**
             * Case when the client asks to switch off the lights and they are already off.
             * Mainly for synchronization purposes.
             * Gives a textual update.
             * Updates the light state variables.
             * Enables the button
             */
            case "lightsAlreadyOff":

                data.putString("msg", "Lights were already off!");
                sendMessage(AppCommons.textUpdate_WHAT, data);

                lightStateBundle = new Bundle();
                lightStateBundle.putString("msg", "OFF");
                sendMessage(AppCommons.updateLightState_WHAT, lightStateBundle);

                buttonStateBundle = new Bundle();
                buttonStateBundle.putString("button", "hit");
                sendMessage(AppCommons.enableButton_WHAT, buttonStateBundle);
                break;

            /**
             * Case when the lights are turned on, be it by any user connected to the server.
             * Updates the light state.
             * Sends a textual update.
             */
            case "lightsTurnedOn":

                lightStateBundle = new Bundle();
                lightStateBundle.putString("msg", "ON");
                sendMessage(AppCommons.updateLightState_WHAT, lightStateBundle);

                buttonStateBundle = new Bundle();
                buttonStateBundle.putString("button","hit");
                sendMessage(AppCommons.enableButton_WHAT,buttonStateBundle);

                data.putString("msg", "Lights were turned on!");
                sendMessage(AppCommons.textUpdate_WHAT, data);
                break;

            /**
             * Case when the lights are turned off, be it by any user connected to the server.
             * Updates the light state.
             * Sends a textual update.
             */
            case "lightsTurnedOff":

                lightStateBundle = new Bundle();
                lightStateBundle.putString("msg", "OFF");
                sendMessage(AppCommons.updateLightState_WHAT, lightStateBundle);

                buttonStateBundle = new Bundle();
                buttonStateBundle.putString("button","hit");
                sendMessage(AppCommons.enableButton_WHAT,buttonStateBundle);

                data.putString("msg", "Lights were turned off!");
                sendMessage(AppCommons.textUpdate_WHAT, data);

                break;

            /**
             * Updates light state without any visual hint
             * Lets button be pressed, if it wasn't already
             */
            case "LightsOnFirstTime":

                lightStateBundle = new Bundle();
                lightStateBundle.putString("msg", "ON");
                sendMessage(AppCommons.updateLightState_WHAT, lightStateBundle);

                buttonStateBundle = new Bundle();
                buttonStateBundle.putString("button", "hit");
                sendMessage(AppCommons.enableButton_WHAT, buttonStateBundle);

                break;

            /**
             * Case when the user tries to turn off the lights and his/hers request is refused.
             * Updates light state.
             * Enables button.
             */
            case "lightsOffRefused":

                data.putString("msg","Couldn't turn off the  lights. You are not alone in the room...");
                sendMessage(AppCommons.textUpdate_WHAT,data);

                lightStateBundle = new Bundle();
                lightStateBundle.putString("msg", "ON");
                sendMessage(AppCommons.updateLightState_WHAT, lightStateBundle);

                buttonStateBundle = new Bundle();
                buttonStateBundle.putString("button", "hit");
                sendMessage(AppCommons.enableButton_WHAT, buttonStateBundle);
                break;

            /**
             * Message received that updates light state.
             * Synching purposes.
             */
            case "LightsOn True":

                lightStateBundle = new Bundle();
                lightStateBundle.putString("msg", "ON");
                sendMessage(AppCommons.updateLightState_WHAT, lightStateBundle);

                data.putString("msg", "Lights were turned on!");
                sendMessage(AppCommons.textUpdate_WHAT, data);

                break;

            case "LightsOn False":
                lightStateBundle = new Bundle();
                lightStateBundle.putString("msg", "OFF");
                sendMessage(AppCommons.updateLightState_WHAT, lightStateBundle);

                data.putString("msg", "Lights were turned off!");
                sendMessage(AppCommons.textUpdate_WHAT, data);

                break;

            /**
             * Logout confirmation message.
             * Used mainly to close the socket at the correct time.
             */
            case "logoutOk":
                sendMessage(AppCommons.toLogin_WHAT, null);
                try {
                    this.socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            /**
             * Shouldn't be needed.
             */
            case "logoutRefused":
                break;

            default:
                Log.i(TAG, "response was: " + response);
                break;
        }
    }

    /**
     * Method to send android.os Messages from this class to the MainActivity.
     *
     * @param what
     * @param data
     */
    protected void sendMessage(int what, Bundle data) {
        Log.i(TAG, "What is: " + what);

        Message msg = msgHandler.obtainMessage(what);
        msg.setData(data);
        msg.sendToTarget();
    }

    /**
     * When client disconnects, this is triggered.
     * Therefore we send a message to exit to the login screen here.
     *
     * @param result
     */
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }

}