package com.example.diogo.automaticlights;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.diogo.automaticlights.Enumerations.FragmentState;
import com.example.diogo.automaticlights.Enumerations.LightState;
import com.example.diogo.automaticlights.Enumerations.Task;
import com.example.diogo.automaticlights.Fragments.LoginFragment;
import com.example.diogo.automaticlights.Fragments.MainFragment;
import com.example.diogo.automaticlights.Interfaces.FragmentInteractionListener;

import static com.example.diogo.automaticlights.AppCommons.textUpdate_WHAT;

public class MainActivity extends AppCompatActivity implements  FragmentInteractionListener{

    /**
     * Connection variables
     */
    private final static String facAddress = "192.168.10.83";
    private final static int port = 8888;

    /**
     * Misc.
     */
    private static final String TAG = "MainActivity";

    /**
     * Fragment managing
     */
    private FragmentState currentFragmentState;
    private FragmentState previousFragmentState;

    /**
     * Fragments
     */
    private LoginFragment loginFragment;
    private MainFragment mainFragment;

    /**
     * Wifi stuff
     */
    WifiManager wifiManager;

    /**
     * Server connection things
     */
    private AsyncCommunication tcpClient;
    private UpdateHandler updateHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        AppCommons.init();

        wifiManager = (WifiManager) this.getApplicationContext().getSystemService(WIFI_SERVICE);

        checkWiFi();

        updateHandler = new UpdateHandler(getMainLooper());

    }

    public String getWifiName(){
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
        if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
            String s = wifiInfo.getSSID();
            Log.i(TAG,"Network ssid is :"+s);
            return s;
        }
        return null;
    }

    private void checkWiFi(){

        Log.i(TAG,"Wifi is enabled: "+wifiManager.isWifiEnabled());

        if(!wifiManager.isWifiEnabled()){
            Log.i(TAG,"Showing dialog...");

            final AlertDialog.Builder wifiAlertDialog = new AlertDialog.Builder(this);

            wifiAlertDialog.setTitle("WiFi enabling");

            wifiAlertDialog.setMessage("So the app functions properly, you need to connect to wifi.");

            wifiAlertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });

            wifiAlertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            wifiAlertDialog.show();

            Log.i(TAG,"dialog shown");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(loginFragment == null)
            loginFragment = new LoginFragment();

        if(mainFragment == null)
            mainFragment = new MainFragment();

        currentFragmentState = null;
        swapFragment(FragmentState.LOGIN);
    }

    @Override
    public void onBackPressed() {
        switch (currentFragmentState){
            case LOGIN:
                tcpClient.closeSocket();
                finish();
                break;

            case MAIN:
                tcpClient.sendMessage("logout");
                swapFragment(FragmentState.LOGIN);
                break;
        }
    }

    /**
     * Generic method to swap between fragments.
     * @param state
     */
    private void swapFragment(FragmentState state) {
        Log.i(TAG,"Entered fragment swap");
        Fragment fragment = null;
        previousFragmentState = currentFragmentState;
        switch (state) {
            case MAIN:
                Log.i(TAG,"Swapped to main fragment");
                fragment = mainFragment;
                break;

            case LOGIN:
                Log.i(TAG,"Swapped to login fragment");
                fragment = loginFragment;
                break;
        }

        currentFragmentState = state;
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.mainActivityLinearLayout, fragment)
                    .commit();
        }
    }

    @Override
    public void doTask(Task t) {
        Log.i(TAG, t.name() + " lots of tasks");
        switch (t){

            case LOGIN:

                tcpClient = new AsyncCommunication(facAddress,port,getApplicationContext(), updateHandler);
                String wifiName = getWifiName().replace("\"", "");
                Log.i(TAG,"Network name is: " + wifiName);
                Log.i(TAG, ""+ wifiName.equals("g8AP")+ " "+ wifiName.length()+" "+ "eduroam".length());
                if(!wifiName.equals("g8AP")){
                    Log.i(TAG,"Network not g8AP, is: "+wifiName);
                    Toast.makeText(this, "Please connect to the correct network", Toast.LENGTH_SHORT).show();
                    break;
                }
                Log.i(TAG,"Network ok, logging in");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!tcpClient.isExecutting()){
                            tcpClient.execute();
                            tcpClient.setExecutting(true);
                            Log.i(TAG,"tcpClient executing!");
                        }
                    }
                });

                while(!tcpClient.isConnected());

                Log.i(TAG, "Logging in...");
                EditText loginIDField = (EditText) findViewById(R.id.loginIDField);
                String id = loginIDField.getText().toString();
                AppCommons.setCurrentUserName(id);
                tcpClient.sendMessage("login "+AppCommons.getCurrentUserName());

                break;

            case HIT_LIGHTS:

                String current = AppCommons.getCurrentLightState().toString();
                switch (current){
                    case "ON":
                        tcpClient.sendMessage("lightsOff "+AppCommons.getCurrentUserName());

                        break;
                    case "OFF":
                        tcpClient.sendMessage("lightsOn "+AppCommons.getCurrentUserName());

                        break;
                }
                break;

            default:
                break;
        }
    }

    /**
     * Method that adds a text update to the main fragment, on a LinearLayout, encapsuled by a scrollview.
     * @param text
     */
    private void addTextUpdate(String text){
        LinearLayout mainFragmentUpdateHub = (LinearLayout) findViewById(R.id.mainFragmentUpdateHub);
        TextView textUpdate = new TextView(this);
        textUpdate.setTextColor(Color.WHITE);
        textUpdate.setText(text);
        mainFragmentUpdateHub.addView(textUpdate);
    }

    /**
     * Method to, hopefully, hide the keyboard on touch.
     * @param activity
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    /**
     * Method to set up the UI, so that the keyboard is hidden if we touch
     * anywhere except for it.
     * @param view
     */
    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(MainActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    /**
     * Updates light state variable and button text.
     * @param lightState, the lightstate to update to.
     */
    private void updateLightState(LightState lightState){
        Log.i(TAG, "Updating light state to "+lightState);
        if(lightState.toString().equals("ON"))
            AppCommons.setCurrentLightState(LightState.ON);
        else
            AppCommons.setCurrentLightState(LightState.OFF);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button hitLightsButton = (Button) findViewById(R.id.hitLightsButton);
                hitLightsButton.setText(AppCommons.getCurrentLightState().toString());
            }
        });


    }

    /**
     * Handles messages received from any class that has the reference to the handler.
     */

    class UpdateHandler extends Handler{

        public UpdateHandler(Looper looper){
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Button loginButton = (Button) findViewById(R.id.sendIDbutton);
            Button hitLightsButton = (Button) findViewById(R.id.hitLightsButton);
            switch (msg.what){
                case AppCommons.toMain_WHAT:
                    Log.i(TAG,"Swapping to main fragment!");
                    swapFragment(FragmentState.MAIN);
                    break;

                case AppCommons.toLogin_WHAT:
                    Log.i(TAG,"Swapping to login fragment!");
                    loginFragment = new LoginFragment();
                    tcpClient.cancel(true);
                    swapFragment(FragmentState.LOGIN);
                    break;

                case AppCommons.updateLightState_WHAT:
                    //hitLightsButton.setEnabled(true);
                    Bundle b = msg.getData();
                    String state = b.getString("msg");
                    if(state.equals("ON"))
                        updateLightState(LightState.ON);
                    else if (state.equals("OFF"))
                        updateLightState(LightState.OFF);
                    break;

                case AppCommons.enableButton_WHAT:
                    Bundle data = msg.getData();
                    String btn = data.getString("button");
                    if(btn.equals("login"))
                        loginButton.setEnabled(true);
                    else if(btn.equals("hit"))
                        hitLightsButton.setEnabled(true);
                    break;

                case AppCommons.disable_button_WHAT:
                    Log.i(TAG,"Disabling button");
                   /* Bundle data1 = msg.getData();
                    String btn1 = data1.getString("button");
                    if(btn1.equals("login"))
                        loginButton.setEnabled(false);
                    else if(btn1.equals("hit"))
                        hitLightsButton.setEnabled(false);*/
                    break;

                case textUpdate_WHAT:
                    Bundle data2 = msg.getData();
                    String str =  data2.getString("msg");
                    addTextUpdate(str);
                    break;

                default:
                    break;
            }
        }
    }

}

