package com.example.diogo.automaticlights.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.diogo.automaticlights.AppCommons;
import com.example.diogo.automaticlights.Enumerations.Task;
import com.example.diogo.automaticlights.Interfaces.FragmentInteractionListener;
import com.example.diogo.automaticlights.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;


public class MainFragment extends Fragment implements View.OnClickListener{

    private FragmentInteractionListener mListener;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView textView =(TextView) getActivity().findViewById(R.id.mainMenuTextView);
        String name = AppCommons.getCurrentUserName();
        textView.setText("Welcome "+ name);
        addListenerToButton(R.id.hitLightsButton);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentInteractionListener) {
            mListener = (FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void addListenerToButton(int id){
        Button button = (Button) getActivity().findViewById(id);
        button.setOnClickListener((View.OnClickListener) this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.hitLightsButton:
                mListener.doTask(Task.HIT_LIGHTS);
                Button button = (Button) getActivity().findViewById(view.getId());
                //button.setEnabled(false);
                break;
            default:
                break;
        }
    }

}



