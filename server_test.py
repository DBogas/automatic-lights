'''
    Simple socket server using threads
'''
import socket
import sys
from thread import *
import serial

HOST = ''   # Symbolic name meaning all available interfaces
PORT = 8888 # Arbitrary non-privileged port
n_pessoas = 0
lightState = 'off' #off/on

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'

#Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()

print 'Socket bind complete'

#Start listening on socket
s.listen(10)
print 'Socket now listening'

#Function for handling connections. This will be used to create threads
def clientthread(conn):
    #Sending message to connected client

    #infinite loop so that function do not terminate and thread do not end.
    while True:

        #Receiving from client
        data = conn.recv(1024)
        reply = 'OK...' + data
        if data != "":
            print data

        message = data.split(" ");
        # se houver database fazer aqui a verificacao
        if(message[0] == "login"):
            print(message[1] + " connected!")
            conn.send("login ok")
        # verificacao da cena das luzes
        elif(message[0] == "lightsOn" or message[0] == "lightsOff"):
            print(message[1] + " wants to hit the lights!")
            conn.send("lights ok")
        


    #came out of loop
    conn.close()

def arduinothread():
    #enviar 'l' = ordem para ligar luz
    #enviar 'd' = ordem para desligar luz
    #
    ser = serial.Serial('/dev/ttyACM0',9600)
    ser.flushInput()
    s = [0,1]
    while True:
        read_serial=ser.readline()
        print 'arduino says ' + str(read_serial)
        if read_serial == "Ligar Luz":
            ser.write('l')


        #receber sinal de android e enviar comando ao arduino
        #if utilizador_autenticado
        #       if android_desligar
        #           ser.write('d')
        #       else if android_ligar
        #           ser.write('l')
        #...
        if read_serial == "Entrou pessoa":
            print read_serial
            n_pessoas += 1
        if read_serial == "Saiu pessoa":
            print read_serial
            if n_pessoas > 0:
                n_pessoas -= 1




#start talking with the arduino
#start_new_thread(arduinothread, ())

#now keep talking with the client
while 1:
    #wait to accept a connection - blocking call
    conn, addr = s.accept()
    print 'Connected with ' + addr[0] + ':' + str(addr[1])

    #start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function.
    start_new_thread(clientthread ,(conn,))

s.close()
