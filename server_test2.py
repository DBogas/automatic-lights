'''
    Simple socket server using threads
'''
import socket
import sys
from thread import *
import serial
import time

HOST = ''   # Symbolic name meaning all available interfaces
PORT = 8888 # Arbitrary non-privileged port
global n_pessoas
global lightsOn
global turnOnRequest
global turnOffRequest
global usersArray
global newUser

usersArray = {'conn':'name'}
n_pessoas = 0
lightsOn = False
usersArray = {'conn':'name'}
turnOnRequest = False
turnOffRequest = False

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'

#Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()

print 'Socket bind complete'

#Start listening on socket
s.listen(10)
print 'Socket now listening'

#Function for handling connections. This will be used to create threads
def clientthread(conn):
    global n_pessoas
    global lightsOn
    global turnOnRequest
    global turnOffRequest
    global usersArray
    global newUser
    #Sending message to connected client

    #infinite loop so that function do not terminate and thread do not end.
    while True:
        #Receiving from client
        data = conn.recv(1024)
        message = data.split(" ")
	if message[0] != "":
			print message

        if message[0] == "login":
            if message[1] not in usersArray.values():
            	usersArray[conn] = message[1]
            	conn.send("loginAccepted")
		conn.send("LightsOn " + str(lightsOn)) 
            	newUser = message[1]
            	time.sleep(4)
            	newUser = ""
		n_pessoas += 1
            else:
            	conn.send("loginRefused")
        elif message[0] == "lightsOn":
        	if lightsOn == True:
        		conn.send("lightsAlreadyOn")
        	elif lightsOn == False:
        		turnOnRequest = True
        		while lightsOn == False:
        			pass
        		conn.send("lightsTurnedOn")	
       	elif message[0] == "lightsOff":
            if n_pessoas == 1:
            	turnOffRequest = True
		while lightsOn == True:
			pass
		conn.send("lightsTurnedOff")
            else:
            	conn.send("lightsOffRefused")
    #came out of loop
    del usersArray[conn]
    conn.close()

def arduinothread():
	global n_pessoas
    	global lightsOn
    	global turnOnRequest
    	global turnOffRequest
    	global usersArray
    	global newUser

	#enviar 'l' = ordem para ligar luz
	#enviar 'd' = ordem para desligar luz
	ser = serial.Serial(
		'/dev/ttyACM0',
		9600,
		parity=serial.PARITY_NONE,
 		stopbits=serial.STOPBITS_ONE,
 		bytesize=serial.EIGHTBITS,
 		timeout=1
 	)
	ser.flushInput()
	s = [0,1]

	while True:
		read_serial=ser.readline()
		#print 'arduino says ' + read_serial[:6] # PRINTTTTT

		if read_serial[:9] == "Ligar Luz" and lightsOn == False: # and n_pessoas > 0: #sensor de luz dispara
			print "entrou na luz"
			ser.write("l")
			lightsOn = True
		elif read_serial[:6] == "Entrou":
			n_pessoas += 1
			#display(newUser)
			if lightsOn == False:
				print "vou ligar a luz dude"
				ser.write("l")
				lightsOn = True
				for key in usersArray.keys(): #TENTA VER SE ISTO ENVIA A TODOS OS TELEMOVEIS 
					key.send("LightsOn " + str(lightsOn))
		elif read_serial[:4] == "Saiu":
			if n_pessoas > 0:
				n_pessoas -= 1
			if n_pessoas == 0:
				ser.write('d')
				lightsOn = False
		elif turnOnRequest == True:
			print "server -> arduino : ligar luz"
			ser.write('l')
			lightsOn = True
			turnOnRequest = False
		elif turnOffRequest == True:
			print "server -> arduino : desligar luz"
			ser.write('d')
			lightsOn = False
			turnOffRequest = False

#start talking with the arduino
#start_new_thread(arduinothread, ())

#now keep talking with the client
while 1:
    #wait to accept a connection - blocking call
    conn, addr = s.accept()
    print 'Connected with ' + addr[0] + ':' + str(addr[1])

    #start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function.
    start_new_thread(clientthread ,(conn,))

s.close()
